﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace overkill
{
    class Body
    {
        public int x { get; set; }
        public int y { get; set; }
        public bool hor { get; set; }
        public bool back { get; set; }

        public Body()
        {
            x = 0;
            y = 0;
            hor = false;
            back = false;
        }
    }
}
