﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace overkill
{
    public partial class Form1 : Form
    {
        private List<Body> Player = new List<Body>();
        private Body point = new Body();
        private List<Body> obstacles = new List<Body>();
        private Body obstacle = new Body();

        int i = 0;
        public Form1()
        {
            InitializeComponent();
            new Prefs();
            gameTimer.Interval = 1000 / Prefs.p_speed;
            gameTimer.Tick += updateScreen;
            gameTimer.Start();
            startGame();
        }

        private void startGame()
        {
            label2.Visible = false;
            new Prefs();
            Player.Clear();
            Body head = new Body { x = 10, y = 5 };
            Player.Add(head);
            label3.Text = Prefs.p_pscore.ToString();
            obstacles.Clear();
            //Circle wall = new Circle { x = 5, y = 3 };
            //obstacles.Add(wall);
            spawn_point();
            spawn_obstacle();
        }

        private void spawn_point()
        {
            int maxXpos = pbCanvas.Size.Width / Prefs.p_width;
            int maxYpos = pbCanvas.Size.Height / Prefs.p_height;
            Random rng = new Random();
            point = new Body { x = rng.Next(10, maxXpos), y = rng.Next(10, maxYpos) };
        }

        private void updateScreen(object sender, EventArgs e)
        {
            if (Prefs.over == true)
            {
                if (Input.KeyPress(Keys.Enter))
                {
                    startGame();
                }
            }
            else
            {

                if (Input.KeyPress(Keys.D))
                {
                    Player[i].x++;
                }
                else if (Input.KeyPress(Keys.A))
                {
                    Player[i].x--;
                }
                else if (Input.KeyPress(Keys.W))
                {
                    Player[i].y--;
                }
                else if (Input.KeyPress(Keys.S))
                {
                    Player[i].y++;
                }

                move();
            }
            pbCanvas.Invalidate();
        }

        private void move()
        {            
            int maxXpos = pbCanvas.Size.Width / Prefs.p_width;
            int maxYpos = pbCanvas.Size.Height / Prefs.p_height;
            move_obstacle();
            if (Player[i].x < 0 || Player[i].y < 0 || Player[i].x > maxXpos || Player[i].y > maxYpos)
            {
                die();
            }


            if (Player[0].x == point.x && Player[0].y == point.y)
            {
                get_point();
                        
            }
            for (int j = 0; j < obstacles.Count; j++)
            {
                if (Player[0].x == obstacles[j].x &&
                    Player[0].y == obstacles[j].y)
                {
                    die();
                }
            }

        } 
                
        private void get_point()
        {
            Prefs.p_pscore += Prefs.p_points;
            label3.Text = Prefs.p_pscore.ToString();
            spawn_point();
            spawn_obstacle();
        }
        
        private void spawn_obstacle()
        {

            int maxXPos = pbCanvas.Size.Width / Prefs.p_width;
            int maxYPos = pbCanvas.Size.Height / Prefs.p_height;

            Random random = new Random();
            Random pst = new Random();
            var n = pst.Next(0, 2);
            Body circle = new Body
            {
                x = random.Next(0, maxXPos),
                y = random.Next(0, maxYPos)            
            };
            if (n % 2 == 0)
            {
                circle.hor = true;
            }
            obstacles.Add(circle);

        }

        private void move_obstacle()
        {
            int maxXpos = pbCanvas.Size.Width / Prefs.p_width;
            int maxYpos = pbCanvas.Size.Height / Prefs.p_height;

            for (int j = 0; j < obstacles.Count; j++)
            {
                if (obstacles[j].hor == true)
                {

                    if (obstacles[j].back == false)
                    {
                        obstacles[j].x++;
                        if (obstacles[j].x == maxXpos)
                        {
                            obstacles[j].back = true;
                            obstacles[j].x--;
                        }
                    }
                    else
                    {
                        obstacles[j].x--;
                        if(obstacles[j].x == maxXpos - 5 - Prefs.p_width)
                        {
                            obstacles[j].back = false;
                        }
                    }
                    
                  
                }
                else
                {
                    if (obstacles[j].back == false)
                    {
                        obstacles[j].y++;
                        if (obstacles[j].y == maxYpos)
                        {
                            obstacles[j].back = true;
                            obstacles[j].y--;
                        }
                    }
                    else
                    {
                        obstacles[j].y--;
                        if (obstacles[j].y == maxXpos - 5 - Prefs.p_height)
                        {
                            obstacles[j].back = false;
                        }
                    }
                }

            }
        }

        private void die()
        {
            Prefs.over = true;
        }

        private void keyisdown(object sender, KeyEventArgs e)
        {
            Input.changeState(e.KeyCode, true);
        }

        private void keyisup(object sender, KeyEventArgs e)
        {
            Input.changeState(e.KeyCode, false);
        }
        
        private void updateGraphics(object sender, PaintEventArgs e)
        {
            Graphics canvas = e.Graphics;

            if (Prefs.over == false)
            {
                Brush player_color;
                    
                player_color = Brushes.Black;
                    
                canvas.FillEllipse(player_color, new Rectangle(Player[i].x * Prefs.p_width, Player[i].y * Prefs.p_height, Prefs.p_width, Prefs.p_height));
                canvas.FillEllipse(Brushes.GreenYellow, new Rectangle(point.x * Prefs.p_width, point.y * Prefs.p_height, Prefs.p_width, Prefs.p_height));
                   
                
                for (int j = 0; j < obstacles.Count; j++)
                {
                    Random random = new Random();
                    canvas.FillRectangle(Brushes.Red,
                        new Rectangle(obstacles[j].x * Prefs.p_width,
                                      obstacles[j].y * Prefs.p_height,
                                      Prefs.p_width, Prefs.p_height));
                }
            }
            else
            {
                string gameOver = "YOU LOST! \n" + "Final Score is " + Prefs.p_pscore + "\n Press enter to Restart \n";
                label2.Text = gameOver;
                label2.Visible = true;
            }
        }
    }
}
