﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace overkill
{
    class Prefs
    {
        public static int p_width { get; set; }
        public static int p_height { get; set; }
        public static int p_speed { get; set; }
        public static int p_pscore { get; set; }
        public static int p_points { get; set; }
        public static bool over { get; set; }
        


        public Prefs()
        {
            p_width = 16;
            p_height = 16;
            p_speed = 20;
            p_pscore = 0;
            p_points = 100;
            over = false;
        }

    }
}
